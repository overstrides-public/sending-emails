# sending-emails

Simple Node.JS application for sending emails via Gmail API.

## .env must contain:

- CLIENT_ID
- CLIENT_SECRET
- REDIRECT_URI
- REFRESH_TOKEN
- EMAILS="..., ..., ..."
