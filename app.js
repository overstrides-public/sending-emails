import { addressList } from './app/address-list.js';
import getTemplateList  from './app/email-templates-list.js';
import { chosenData } from './app/choosing-data.js';
import { sendingEmailsLoop } from './app/sending-emails-loop.js'

async function main() {
  const userSettings = {
    addressList: addressList,
    templatesList: await getTemplateList([])
  };
  const response = await chosenData(userSettings);
  await sendingEmailsLoop(response);
}

main()
  .then(result => console.log('\n ✔ DONE'));