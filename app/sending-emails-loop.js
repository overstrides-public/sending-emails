import getEmailBody from './getting-email-body.js';
import getEmailSubject from './getting-email-subject.js';
import sendingEmail from './sending-email.js';

export async function sendingEmailsLoop ({ addressList, templatesList }) {
  const emailsQty = templatesList.length;
  for (const [templateIndex, template] of templatesList.entries()) {
    const emailBody = await getEmailBody(template);
    const emailSubject = await getEmailSubject(emailBody)
    await sendingEmail(addressList, emailSubject, emailBody, templateIndex, emailsQty)
  }
};