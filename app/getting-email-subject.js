import HTMLParser from 'node-html-parser';

export default async function getEmailSubject(body) {
  return HTMLParser.parse(body).querySelector('title').text;
}