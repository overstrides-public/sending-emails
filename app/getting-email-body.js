import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const currentLocation = path.dirname(fileURLToPath(import.meta.url));

export default async function getEmailBody(template) {
  return fs.readFileSync(path.join(currentLocation, '../../..', template));
}