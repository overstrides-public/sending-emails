import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const currentLocation = path.dirname(fileURLToPath(import.meta.url));

export default async function getTemplateList(templatesList) {
  const files = fs.readdirSync(path.join(currentLocation, '../../..'));
  const templates = [...files.filter((file => path.extname(file) === '.html'))];
  return [...templates.map(template => ({ title: template, value: template }))];
}



