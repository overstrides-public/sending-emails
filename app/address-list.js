import 'dotenv/config';

export const addressList = process.env.EMAILS
  .split(", ")
  .map(address => ({ title: address, value: address }));