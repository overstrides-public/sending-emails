import { oAuth2Client } from './oath.js';
import nodemailer from 'nodemailer';
import 'dotenv/config';
import dedent from 'dedent';

export default async function sendingEmail ([to, ...cc], emailSubject, emailBody, templateIndex, emailsQty) {

  try {
    const accessToken = await oAuth2Client.getAccessToken();
    const transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: 'dev.o.shut@gmail.com',
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken
      }
    });

    const mailOptions = {
      from: 'Oleksandr Shut <dev.o.shut@gmail.com>',
      to: to,
      subject: emailSubject,
      html: emailBody,
      text: 'The Gmail API with NodeJS works'
    };
    (cc.length > 0) && Object.assign(mailOptions, { cc: [...cc] });

    await transport.sendMail(mailOptions);

    console.log(dedent`\n
        SENT ${templateIndex+1}/${emailsQty}
        to: ${mailOptions.to}${mailOptions.cc ? `, cc: ${mailOptions.cc}` : ``}
        subject: ${mailOptions.subject}`);

  } catch (error) {
    console.log(error);
  }

}