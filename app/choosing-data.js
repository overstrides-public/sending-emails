import prompts from 'prompts';

export const chosenData = async ({ addressList, templatesList }) => {
  return await prompts([
    {
      type: 'multiselect',
      name: 'addressList',
      message: 'Select emails',
      choices: addressList,
      min: 1
    },
    {
      type: 'multiselect',
      name: 'templatesList',
      message: 'Select templates',
      choices: templatesList,
      min: 1
    }]);
};